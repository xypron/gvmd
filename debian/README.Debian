Notes about gvmd
----------------

On Kali / Debian we highly recommend to install the package gvm and to use
the commands provided by the package gvm to setup correctly GVM tools:
'sudo gvm-setup'
'sudo gvm-check-setup'

(see more information in /usr/share/doc/gvm/README.Debian)


How to update the data in Kali / Debian
---------------------------------------
Use one of the following commands (depending on what you want to update):
    'sudo greenbone-feed-sync --type GVMD_DATA'
    'sudo greenbone-feed-sync --type SCAP'
    'sudo greenbone-feed-sync --type CERT'
    'sudo -u _gvm greenbone-nvt-sync'
Please note that you need to run the latest command as _gvm user (see
below for more information)

If you have installed the package gvm you can update the data (CERT, SCAP,
GVMD_DATA) from the feed server and the OpenVAS NVTs from Community Feed
with one command:
    'sudo gvm-feed-update'


How to run other GVM commands in Kali / Debian
-------------------------------------------------

Since gvm 11, we use a special user _gvm.

You need to use this user to run the GVM commands, like

'sudo -u _gvm gvmd --get-scanners'
'sudo -u _gvm gvmd *'

'sudo -u _gvm greenbone-nvt-sync'

NOTE: if you run commands only with sudo (or as root user), created or
updated files may by owned by root instead of _gvm user and your
installation will not work correctly.


Other information
-----------------

Postgresql optimization: with latest versions of postgresql gvm can be
very slow.
It can help to add "jit = off" in the postgresql.conf
https://community.greenbone.net/t/everything-works-but-i-cant-see-any-report/5875/13
https://bugs.kali.org/view.php?id=6763
